<?php

return [
    'defaults'  => [
        'log_file'      => env('EVENTS_AUDIT_LOG_FILE', storage_path('logs/audit.log')),
        'log_channel'   => env('EVENTS_AUDIT_LOG_CHANNEL', 'EventsAuditLoggingListener'),
        'listener'      => 'ThisWay\EventsAudit\Listeners\LoggingListener',
    ],
    'events_to_log' => [
        'ThisWay\EventsAudit\Events\UserCreatedEvent',
        'ThisWay\EventsAudit\Events\UserDeletedEvent',
    ],
];
