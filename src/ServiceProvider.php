<?php

namespace ThisWay\EventsAudit;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Monolog\Handler\StreamHandler;
use ThisWay\EventsAudit\Formatters\JsonFormatter;

class ServiceProvider extends EventServiceProvider
{
    public function register()
    {
        $this->app->singleton('ThisWay\EventsAudit\LoggerService', function($app)
        {
            $logger     = new LoggerService(config('events-audit.defaults.log_channel'));
            $handler    = new StreamHandler(config('events-audit.defaults.log_file'));

            $handler->setFormatter(new JsonFormatter());
        
            $logger->pushHandler($handler);
        
            return $logger;
        });
    }

    /**
     * Register any other events for your application.
     *
     * @param  Dispatcher  $events
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $this->publishes([
            __DIR__ . '/../config/events-audit.php'    => config_path('events-audit.php'),
        ], 'config');

        // Load the listener-events pairs
        $defaultListener    = config('events-audit.defaults.listener');
        $eventsToLog        = (array)config('events-audit.events_to_log');

        if (count($eventsToLog))
        {
            foreach ($eventsToLog as $event => $listener)
            {
                if (!is_int($event))
                {
                    $this->listen[$event][] = $listener;
                }
                else
                {
                    $this->listen[$listener][]  = $defaultListener;
                }
            }
        }

        parent::boot($events);
    }
}