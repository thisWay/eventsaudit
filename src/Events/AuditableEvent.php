<?php

namespace ThisWay\EventsAudit\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

abstract class AuditableEvent extends Event
{
    use SerializesModels;

    protected $data = [];

    public function __construct(array $data = null)
    {
        if (!is_null($data))
        {
            $this->setData($data);
        }
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData(array $data)
    {
        $this->data = array($data);

        return $this;
    }
}