<?php

namespace ThisWay\EventsAudit\Listeners;

use Illuminate\Http\Request;
use Monolog\Logger;
use LucaDegasperi\OAuth2Server\Authorizer;
use ThisWay\EventsAudit\Events\AuditableEvent;
use ThisWay\EventsAudit\LoggerService;

class LoggingListener
{
    /** @var  Request */
    protected $request;
    /** @var  Authorizer */
    protected $authorizer;
    /** @var  Logger */
    protected $logger;

    /**
     * Create the event listener.
     *
     * @param Authorizer $authorizer
     * @param Request $request
     * @param LoggerService $logger
     */
    public function __construct(Authorizer $authorizer, Request $request, LoggerService $logger)
    {
        $this->logger       = $logger;
        $this->authorizer   = $authorizer;
        $this->request      = $request;
    }

    /**
     * Handle the event.
     *
     * @param  AuditableEvent $event
     * @return void
     */
    public function handle(AuditableEvent $event)
    {
        $eventData  = [
            'datetime'      => date('Y-m-d H:i:s'),
            'client_id'     => $this->authorizer->getClientId(),
            'ip'            => $this->request->header('T-Forwarded-User-IP', 'anonymous'),
            'user-agent'    => $this->request->header('T-Forwarded-User-Agent', 'anonymous'),
            'event'         => substr(strrchr(get_class($event), '\\'), 1),
            'request-owner' => $this->request->header('T-Forwarded-Request-Owner', 'anonymous'),
            'data'          => $event->getData(),
        ];

        $message    = json_encode($eventData);

        $this->logger->info($message);
    }

}