<?php

namespace ThisWay\EventsAudit\Formatters;

use Monolog\Formatter\JsonFormatter as MonologJsonFormatter;

/**
 * Class JsonFormatter
 *
 * Expands and adds the JSON encoded object supplied as 'message' to the standard record array
 */
class JsonFormatter extends MonologJsonFormatter
{
    public function format(array $record)
    {
        $formatted  = get_object_vars(json_decode($record['message']));

        unset($record['message']);

        $formatted  += $record;

        return $this->toJson($this->normalize($formatted), true) . ($this->appendNewline ? "\n" : '');
    }
}